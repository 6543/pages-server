{
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    systems,
  }:
    flake-utils.lib.eachSystem (import systems)
    (system: let
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      devShells.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          glibc.static
          go
          gofumpt
          golangci-lint
          gopls
          gotools
          go-tools
          sqlite-interactive
        ];
      };
    });
}
